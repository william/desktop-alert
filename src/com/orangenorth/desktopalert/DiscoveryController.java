package com.orangenorth.desktopalert;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import javax.bluetooth.BluetoothStateException;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

public class DiscoveryController {
    private static final Logger logger = Logger.getLogger(DiscoveryController.class.getName());

    @FXML
    private GridPane pane;

    @FXML
    private ListView deviceList;

    private ExecutorService pool;

    public DiscoveryController() {
        startup();
    }

    @FXML
    private void discoverDevices(ActionEvent actionEvent) {
        try {
            pool.execute(new PhoneDiscovery(new PhoneDiscovery.DiscoveryCallback() {
                @Override
                public void discovered(List<PhoneDiscovery.DiscoveredDevice> devices) {
                    final ObservableList<PhoneDiscovery.DiscoveredDevice> content =
                            FXCollections.observableArrayList(devices);
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            deviceList.setItems(content);
                        }
                    });
                }
            }));
        } catch (BluetoothStateException e) {
            // Something went wrong.
        }
    }

    @FXML
    private void selectDevice(ActionEvent actionEvent) {
        final Stage mainStage = (Stage) pane.getScene().getWindow();
        mainStage.hide();
        shutdown();

        try {
            URL location = getClass().getResource("res/connector.fxml");
            final FXMLLoader fxmlLoader = new FXMLLoader(location);
            GridPane pane = (GridPane) fxmlLoader.load();
            mainStage.setScene(new Scene(pane));
            mainStage.setTitle("Hello");

            final ConnectorController controller = fxmlLoader.getController();
            PhoneDiscovery.DiscoveredDevice device = (PhoneDiscovery.DiscoveredDevice) deviceList.
                    getSelectionModel().getSelectedItem();
            controller.establishConnection(device.url);

            mainStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent windowEvent) {
                    controller.shutdown();
                }
            });

            mainStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void startup() {
        pool = Executors.newSingleThreadExecutor();
    }

    public void shutdown() {
        logger.info("DiscoveryController shutdown.");
        pool.shutdown();
    }
}
