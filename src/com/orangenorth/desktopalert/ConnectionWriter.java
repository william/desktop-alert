package com.orangenorth.desktopalert;

import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.SynchronousQueue;
import java.util.logging.Logger;

public class ConnectionWriter extends Thread {
    private static final Logger logger = Logger.getLogger(ConnectionWriter.class.getName());

    private final SynchronousQueue<byte[]> outboundData;
    private final OutputStream outputStream;

    public ConnectionWriter(OutputStream outputStream) {
        this.outputStream = outputStream;
        outboundData = new SynchronousQueue<>();
    }

    @Override
    public void run() {
        try {
            logger.info("Started running ConnectionWriter.");
            while (true) {
                byte[] outData = outboundData.take();
                outputStream.write(outData);
                outputStream.flush();
            }
        } catch (InterruptedException | IOException e) {
            // Can't do anything. Abort.
        }
        logger.info("Shutting down ConnectionWriter.");
    }

    public void shutdown() {
        try {
            logger.info("Preparing to close ConnectionWriter.");
            outputStream.close();
        } catch (IOException e) {
            // Can't do anything useful here.
        }

        interrupt();
    }

    public void keepAlive() {
        outboundData.add(new byte[]{0});
    }
}
