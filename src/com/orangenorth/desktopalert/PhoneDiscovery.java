package com.orangenorth.desktopalert;

import javax.bluetooth.*;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class PhoneDiscovery implements Runnable {
    private static final UUID serviceUUID = new UUID("fd8b01e9691e4f78bc74ea185209239d", false);
    private DiscoveryCallback callback;
    private LocalDevice localDevice;
    private DiscoveryListener listener;

    public PhoneDiscovery(DiscoveryCallback callback) throws BluetoothStateException {
        this.callback = callback;
        listener = new PhoneDiscoveryListener();
        localDevice = LocalDevice.getLocalDevice();
    }

    @Override
    public void run() {
        try {
            localDevice.getDiscoveryAgent().startInquiry(DiscoveryAgent.GIAC, listener);
        } catch (BluetoothStateException e) {
            // If the bluetooth stuff fails, just send back an empty list.
            callback.discovered(new LinkedList<DiscoveredDevice>());
        }
    }

    private class PhoneDiscoveryListener implements DiscoveryListener {
        private List<RemoteDevice> devicesDiscovered;
        private List<DiscoveredDevice> supportedDevices;

        public PhoneDiscoveryListener() {
            devicesDiscovered = new LinkedList<>();
            supportedDevices = new LinkedList<>();
        }

        @Override
        public void deviceDiscovered(RemoteDevice btDevice, DeviceClass cod) {
            devicesDiscovered.add(btDevice);
        }

        @Override
        public void inquiryCompleted(int discType) {
            queryServiceOnNextDevice();
        }

        @Override
        public void servicesDiscovered(int transId, ServiceRecord[] serviceRecords) {
            for (ServiceRecord record : serviceRecords) {
                String url = record.getConnectionURL(ServiceRecord.NOAUTHENTICATE_NOENCRYPT, false);
                if (url == null) {
                    continue;
                }

                try {
                    supportedDevices.add(new DiscoveredDevice(
                            record.getHostDevice().getFriendlyName(false), url));
                } catch (IOException e) {
                    // Can't do much if something goes wrong here.
                }
            }
        }

        @Override
        public void serviceSearchCompleted(int transId, int respCode) {
            queryServiceOnNextDevice();
        }

        private void queryServiceOnNextDevice() {
            UUID[] searchUuidSet = new UUID[]{
                    serviceUUID
            };

            int[] attrIds = new int[]{
                    0x0100 // Service name
            };

            // We are done.
            if (devicesDiscovered.isEmpty()) {
                callback.discovered(supportedDevices);
                return;
            }

            // Query the next device that doesn't throw an exception.
            while (true) {
                try {
                    RemoteDevice device = devicesDiscovered.remove(0);
                    localDevice.getDiscoveryAgent().searchServices(attrIds,
                            searchUuidSet,
                            device, this);
                    break;
                } catch (BluetoothStateException e) {
                    // If we encounter an exception here, we can't really do much. Move
                    // on to the next device.
                }
            }
        }
    }

    public interface DiscoveryCallback {
        void discovered(List<DiscoveredDevice> devices);
    }

    public class DiscoveredDevice {
        public String deviceName;
        public String url;

        DiscoveredDevice(String deviceName, String url) {
            this.deviceName = deviceName;
            this.url = url;
        }

        @Override
        public String toString() {
            return deviceName;
        }
    }
}
