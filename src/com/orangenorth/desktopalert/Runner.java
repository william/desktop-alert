package com.orangenorth.desktopalert;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.net.URL;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Runner extends Application {
    private static final Logger logger = Logger.getLogger(Runner.class.getName());

    @Override
    public void start(Stage stage) throws Exception {
        URL location = getClass().getResource("res/deviceselector.fxml");
        final FXMLLoader fxmlLoader = new FXMLLoader(location);
        GridPane pane = (GridPane) fxmlLoader.load();
        stage.setScene(new Scene(pane));
        stage.setTitle("Hello");
        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent windowEvent) {
                DiscoveryController controller = fxmlLoader.getController();
                controller.shutdown();
            }
        });
        stage.show();
        logger.info("Application started.");
    }

    public static void main(String[] args) {
        Logger.getGlobal().addHandler(new ConsoleHandler());
        Logger.getGlobal().setLevel(Level.INFO);
        launch(args);
    }

}
