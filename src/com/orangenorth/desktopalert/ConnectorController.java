package com.orangenorth.desktopalert;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class ConnectorController {
    @FXML
    private Label phoneNumberLabel;

    private static final Logger logger = Logger.getLogger(ConnectorController.class.getName());
    private StreamConnection streamConnection;
    private ConnectionReader reader;
    private ConnectionWriter writer;
    private ScheduledExecutorService scheduler;

    public void establishConnection(final String url) {
        Thread handshakeConnector = new HandshakeConnector(url);
        handshakeConnector.start();
        scheduler = Executors.newScheduledThreadPool(1);
    }

    public void shutdown() {
        logger.info("Shutting down ConnectorController.");

        if (streamConnection != null) {
            try {
                streamConnection.close();
            } catch (IOException e) {
                // Nothing useful to do here.
            }
        }

        if (reader != null) {
            reader.shutdown();
        }

        if (writer != null) {
            writer.shutdown();
        }

        scheduler.shutdown();
    }

    public void notifyPhoneCall(final String phoneNumber) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                phoneNumberLabel.setText(phoneNumber);
                String path = getClass().getResource("res/ring.mp3").toString();
                Media ring = new Media(path);
                MediaPlayer mediaPlayer = new MediaPlayer(ring);
                mediaPlayer.play();
            }
        });
    }

    private class HandshakeConnector extends Thread {
        private final String url;

        public HandshakeConnector(String url) {
            this.url = url;
        }

        @Override
        public void run() {
            try {
                streamConnection = (StreamConnection) Connector.open(url);
                OutputStream outputStream = streamConnection.openOutputStream();
                InputStream inputStream = streamConnection.openInputStream();

                String handshakeString = "BluetoothPhoneNotifications Version: 1";
                byte[] handshake = handshakeString.getBytes(Charset.forName("UTF-8"));

                outputStream.write(handshake);
                outputStream.flush();

                byte[] handshakeReceive = new byte[handshake.length];
                inputStream.read(handshakeReceive);

                if (Arrays.equals(handshake, handshakeReceive)) {
                    logger.info("Handshake accepted, preparing channels.");

                    reader = new ConnectionReader(ConnectorController.this, inputStream);
                    reader.start();

                    writer = new ConnectionWriter(outputStream);
                    writer.start();

                    scheduler.scheduleAtFixedRate(new ConnectionKeeper(), 30, 30, TimeUnit.SECONDS);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private class ConnectionKeeper implements Runnable {
        @Override
        public void run() {
            writer.keepAlive();
        }
    }
}
