package com.orangenorth.desktopalert;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.logging.Logger;

public class ConnectionReader extends Thread {
    private static Logger logger = Logger.getLogger(ConnectionReader.class.getName());
    private final ConnectorController controller;
    private final InputStream inputStream;

    public ConnectionReader(ConnectorController controller, InputStream inputStream) {
        this.controller = controller;
        this.inputStream = inputStream;
    }

    @Override
    public void run() {
        try {
            logger.info("ConnectionReader set up and ready to read.");
            while (true) {
                int command = inputStream.read();
                logger.info("Command read.");
                dispatch(command, inputStream);
            }
        } catch (IOException e) {
            // Quit.
        }
        logger.info("ConnectionReader shutting down.");
    }

    private void dispatch(int command, InputStream inputStream) throws IOException {
        switch (command) {
            case 1:
                logger.info("Phone call command received.");
                // Phone call, get number and create notification.
                int lengthOfPhoneNumber = inputStream.read();
                byte[] phoneNumber = new byte[lengthOfPhoneNumber];
                logger.info("Reading phone number");
                inputStream.read(phoneNumber);
                controller.notifyPhoneCall(new String(phoneNumber, Charset.forName("UTF-8")));
                break;
        }
    }

    public void shutdown() {
        try {
            inputStream.close();
        } catch (IOException e) {
            // Can't do anything useful.
        }
    }
}
